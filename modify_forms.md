# Модификация форм базовой конфигурации расширениями

## Цель

Cнизить возможные риски при наличии модифицируемых расширениями форм

## Предпосылки

При построении исполняемой среды 1С "объединяет" основную конфигурацию и модификации заимствованных объектов в расширениях. При объединении структур форм, **особенно** при наличии **нескольких** расширений, меняющих одну форму наблюдаются "плавающие" ошибки времени выполнения. Модифицированная форма ведет себя непредсказуемо и возникают непрогнозируемые инциденты.

## Решение

* Заимствованные формы **должны быть пустыми**. Т.е. не должны содержать ни одного элемента/реквизита формы.
* Всю модификацию формы производим **только кодом в модуле формы**

Это дает, кроме исключения непрогнозируемых платформенных глюков при объединении структур форм основной конфигурации и расширений, такие профиты

* Изменения формы в явном виде прописаны в коде, контролируемы (изменения, внесенные на саму форму неочевидны).
* Снижение нагрузочных издержек платформы при создании форм - нет необходимости производить анализ/объединение структур.

Минус - повышение времени разработки. Очевидно, что потыкать мышкой на форме быстрее, чем написать код модификации формы.

### Технически

Идея состоит в том, что первичное заимствование форм должно происходить в *пустой* конфигурации. После этого дальнейшая работа с заимствованный формой (в расширении) идет уже в основной конфигурации. На форму **никогда** не добавляются **никакие** элементы/реквизиты в явном виде, **только программно**.

Предполагается, что исходно имеем конфигурацию и расширение, подключенные к собственным хранилищам stor_conf и stor_ext.

### Предварительно

**ВАЖНО** В свойствах расширения **обязательно** должна быть выключена опция "*Поддерживать соответствие объектам расширяемой конфигурации по внутренним идентификаторам*". Практика показывает, что лучше вообще снять все возможные изменяемые значения для корня расширения.

1. Создаем хранилище *stor_forms_ext* с пустой конфигурацией. В нем будут содержаться *пустые болванки* базовых для заимствования объектов.
    > Возможная ошибка, если расширение тоже создается в пустой базе. При первичном импорте расширения возможна проблема сохранения *"Значение контролируемого свойства ОбъектРасширяемойКонфигурации у объекта Язык.Русский не совпадает со значением в расширяемой конфигурации"* - в гиперссылке *Действие - Исправить* выбрать *"Сохранить имя, изменив соответствие"*.
2. В хранилище расширения заводим дублирующих основным пользователей вида *user_form*

### Процесс

```mermaid
graph TB

    names_form[Пользователи хранилищ вида user_form]
    names_common[Обычные пользователи хранилищ]

    create(Создаем форму)
    import(Заимствуем форму в расширение)
    put(Помещаем в хранилища)
    get(Получаем пустые заимствованные объекты)
    work(Продолжаем разработку в обычном режиме)

    subgraph "База пустых объектов"
    names_form .- create
    create --> import
    import --> put
    end

    subgraph "Основная база конфигурации"
    put.- names_common .->get
    get --> work
    end

```

* Хранилище пустых объектов расширения, пользователи хранилищ вида *user_form*
  1. В конфигурации создаем одноименные с основной конфигурацией объекты для заимствования.
      > **ВАЖНО**: При создании форм объектов необходимо создавать произвольные формы для исключения автоматического создания реквизита формы со ссылкой на объект. Форма должна быть **абсолютно пустой**!
  2. Заимствуем созданное в расширение.
  3. Помещаем сделанное в хранилища пустой конфигурации и расширения
* Основная конфигурация, пользователи хранилища расширения обычного вида
  1. Получаем пустые заимствованные объекты.
  2. Продолжаем работу в обычном режиме.

В картинках

Пустой объект и его форма в конфигурации "болванок"

[![Объекты в конфигурации болванок](images/modify_forms/create_preview.png)](images/modify_forms/create.png "Объекты в конфигурации болванок")

Заимствованный объект и форма в расширении

[![Объект и форма в расширении](images/modify_forms/import_preview.png)](images/modify_forms/import.png "Объект и форма в расширении")