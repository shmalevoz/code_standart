# <img src="./images/07_analytic.png" align=top height=25 width=25/> 7. Запросы

## Правила

* **7.1** Основной принцип оформления запросов – ориентируемся на результат работы конструктора запросов.
* **7.2** Следует начинать код запроса с новой строки и пользоваться табуляцией, а не пробелами для отступа в начале строки. *Например:*
  > <details><summary>Развернуть</summary>
  > Правильно:
  > 
  > ```bsl
  > Запрос	= Новый Запрос("
  > |ВЫБРАТЬ
  > |	Ссылка
  > |ИЗ
  > |	Справочник.Валюты
  > |");
  > ```
  > 
  > Неправильно:
  > 
  > ```bsl
  > Запрос	= Новый Запрос("ВЫБРАТЬ
  > |	Ссылка
  > |ИЗ
  > |	Справочник.Валюты
  > |");
  > ```
  > 
  > Также неправильно писать
  > 
  > ```bsl
  > Запрос	= Новый Запрос();
  > Запрос.Текст = 
  > "
  > ```
  > 
  > т.е. текст запроса следует размещать прямо в конструкторе. Понятно, что это не относится к сложным случаям динамической сборки запроса. 
* **7.3** Рекомендуется не указывать псевдонимы, если они совпадают с именем поля/таблицы, для повышения читабельности запроса.
* **7.4** Текст запроса должен быть структурирован, не следует писать запрос в одну строку.
* **7.5** Следует использовать осмысленные псевдонимы таблиц, особенно при длинных именах, предлагаемых конструктором запроса. *Например:* `РаспределениеНастройка.*` вместо `БазаРаспределенияЗатратМеждународныйУчетСрезПоследних.*` при получении данных `РС.БазаРаспределенияЗатратМеждународныйУчет`
* **7.6** В общем случае тексты запросов необходимо оформлять отдельными методами. Таким образом уменьшается объем целевого метода и отделяется логика запроса от дальнейшей обработки его результатов.
* **7.7** В текстах динамически формируемых или сложных запросов запросов (запрос состоит из более чем 5ти выборок и/или временных таблиц и/или соединений) обязательно наличие лидирующего описания запроса и примерный план его выполнения, размещенные комментарием в начале текста запроса. Описание пишется в произвольном виде и должно отражать суть происходящего. *Например:*
  > <details><summary>Развернуть</summary>
  > 
  > ```bsl
  > // Сверка локального и внешнего состава документов
  > //
  > // Сверяем состав РС.СверкаВнешнихДанных[Таблицы|Шапки] с существующими ПРОВЕДЕННЫМИ документами
  > // необходимо, чтобы состав регистра по ключам
  > //	Серия + Количество 
  > // совпадал с данными табличной части документа
  > // алгоритм сверки по шагам
  > //	для каждого следующего шага сверка производится среди данных, не включенных в предыдущие результаты
  > //
  > // Имена таблиц ошибок
  > //	Проверка 1.1 - Документ НЕ проведен - ТаблицаДокументыНеПроведены
  > //	Проверка 1.3 - Серийный состав одинаков внутри дня, не разрывая документы - ТаблицаДниСоставыРавны
  > //	Проверка 1.2 - Разница дат документов - ТаблицаОшибкаДатыРазличны
  > //	Проверка 2 - Различающиеся табличные части - ТаблицаСсылкиЕстьСоставыРазличны
  > //	Проверка 3 - Сведение безссылочных данных - ТаблицаСсылкиОтсутствуютСоставыРавны
  > //	Проверка 4 - Оставшиеся "лишние" локальные документы - ТаблицаДокументыИзлишки
  > //	Проверка 5 - Оставшиеся "лишние" внешние данные - ТаблицаЗаписиИзлишки
  > //
  > // Имена постоянных псевдонимов таблиц
  > //	Шапки - РС.СверкаВнешнихДанныхШапки
  > //	Набор - РС.СверкаВнешнихДанныхТаблицы
  > //	Документы - данные шапки документов
  > //	Состав - данные табличной части документов
  > //
  > // Постфиксы сравниваемых полей
  > //	Лок - Локальные данные
  > //	Внш - Внешние данные
  > ```
  > 
  > </details>
* **7.8** При обращении к свойствам поля составного типа следует применять выражение ВЫРАЗИТЬ для явного указания типа поля.
  > При обращении к свойству поля составного типа происходит неявное соединение со всеми таблицами, в которых может содержаться значение этого поля.
* **7.9** В общем случае, при объединении в запросе результатов нескольких запросов следует использовать конструкцию `ОБЪЕДИНИТЬ ВСЕ`, а не `ОБЪЕДИНИТЬ`.
Поскольку во втором варианте, при объединении запросов полностью одинаковые строки заменяются одной, на что затрачивается дополнительное время, даже в случаях, когда одинаковых строк в запросах заведомо быть не может.
Исключением являются ситуации, когда выполнение замены нескольких одинаковых строк одной является необходимым условием выполнения запроса.
* **7.10** Конструкцию `ДЛЯ ИЗМЕНЕНИЯ `надо применять только к таблицам, для которых стоит автоматический режим управления блокировкой данных, т. к. для таблиц с управляемым режимом она не работает.
* **7.11** Выборку из результатов запроса необходимо выполнять только при условии, если результат запроса содержит данные. Проверку того, что результат выполнения запроса не содержит строк, следует выполнять с помощью метода результата запроса `Пустой()`.
* **7.12** При использовании конструктора необходимо позаботиться о сохранении ранее внесенных в запрос комментариев.
* **7.13** Константные значения (пустые даты, ссылки, предопределенные значения, ...), необходимо располагать в теле запроса, без использования параметров.
* **7.14** Запрещается использовать в запросах соединения с подзапросами, в том числе с виртуальными таблицами. [Пояснения на ИТС](https://its.1c.ru/db/metod8dev/content/5842/hdoc). Исключением являются места, где отсутствует возможность создания временных таблиц - RLS, динамические списки, ..., при этом такие запросы обязательно должны проходить внимательную модерацию.
* **7.15** Необходимо использовать префикс `Таблица` для имен временных таблиц

---

[6. Управляющие конструкции](./code_standart_06_control_structs.md) <== Перейти ==> [8. Объекты метаданных](./code_standart_08_metadata_objects.md)